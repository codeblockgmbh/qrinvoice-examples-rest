Sub Call_RestAPI()

    Dim JsonInput As String
    Dim RestUrl As String
    Dim AsyncRequest As Boolean
    Dim FileNumber As Long
    Dim ResponseBytes() As Byte
    
    Dim OutputFile As String
    
    ChDir ActiveWorkbook.Path
    OutputFile = ActiveWorkbook.Path & "/paymentPartReceipt.png"
    
    ' Target URL of the REST API including the parameter
    ' The following URL points to the demo environment. For production use with individual API key switch URL to https://rest.qr-invoice.cloud/...
    RestUrl = "https://demo.qr-invoice.cloud/v2/payment-part-receipt"
    ' This is an API Key for demonstration purposes. Some values are overridden during create of QR Payment Part & Receipt
    RestUrl = RestUrl & "?api_key=582c9ea9-741a-4bb6-acae-cf92f8805864"
    ' Boundary Lines are not needed when printed on perforated paper
    RestUrl = RestUrl & "&boundaryLineScissors=true"
    RestUrl = RestUrl & "&boundaryLineSeparationText=false"
    RestUrl = RestUrl & "&boundaryLines=true"
    ' On the demo instance, only Liberation Sans is supported
    RestUrl = RestUrl & "&fontFamily=LIBERATION_SANS"
    ' DIN_LANG, A5, A4
    RestUrl = RestUrl & "&pageSize=DIN_LANG"
    ' Resolution only needed for rasterized graphics
    RestUrl = RestUrl & "&resolution=MEDIUM_300_DPI"

    ' The json document contains the information to be rendered
    JsonInput = "{" & vbCrLf
    JsonInput = JsonInput & "  ""creditorInformation"": {" & vbCrLf
    JsonInput = JsonInput & "    ""iban"": ""CH4431999123000889012""," & vbCrLf
    JsonInput = JsonInput & "    ""creditor"": {" & vbCrLf
    JsonInput = JsonInput & "      ""addressType"": ""STRUCTURED""," & vbCrLf
    JsonInput = JsonInput & "      ""name"": ""Robert Schneider AG""," & vbCrLf
    JsonInput = JsonInput & "      ""streetName"": ""Rue du Lac""," & vbCrLf
    JsonInput = JsonInput & "      ""houseNumber"": ""1268""," & vbCrLf
    JsonInput = JsonInput & "      ""postalCode"": ""2501""," & vbCrLf
    JsonInput = JsonInput & "      ""city"": ""Biel""," & vbCrLf
    JsonInput = JsonInput & "      ""country"": ""CH""" & vbCrLf
    JsonInput = JsonInput & "    }" & vbCrLf
    JsonInput = JsonInput & "  }," & vbCrLf
    JsonInput = JsonInput & "  ""paymentAmountInformation"": {" & vbCrLf
    JsonInput = JsonInput & "    ""amount"": 1949.75," & vbCrLf
    JsonInput = JsonInput & "    ""currency"": ""CHF""" & vbCrLf
    JsonInput = JsonInput & "  }," & vbCrLf
    JsonInput = JsonInput & "  ""ultimateDebtor"": {" & vbCrLf
    JsonInput = JsonInput & "    ""addressType"": ""STRUCTURED""," & vbCrLf
    JsonInput = JsonInput & "    ""name"": ""Pia-Maria Rutschmann-Schnyder""," & vbCrLf
    JsonInput = JsonInput & "    ""streetName"": ""Grosse Marktgasse""," & vbCrLf
    JsonInput = JsonInput & "    ""houseNumber"": ""28""," & vbCrLf
    JsonInput = JsonInput & "    ""postalCode"": ""9400""," & vbCrLf
    JsonInput = JsonInput & "    ""city"": ""Rorschach""," & vbCrLf
    JsonInput = JsonInput & "    ""country"": ""CH""" & vbCrLf
    JsonInput = JsonInput & "  }," & vbCrLf
    JsonInput = JsonInput & "  ""paymentReference"": {" & vbCrLf
    JsonInput = JsonInput & "    ""referenceType"": ""QRR""," & vbCrLf
    JsonInput = JsonInput & "    ""reference"": ""210000000003139471430009017""," & vbCrLf
    JsonInput = JsonInput & "    ""additionalInformation"": {" & vbCrLf
    JsonInput = JsonInput & "      ""unstructuredMessage"": ""Instruction of 03.04.2019""," & vbCrLf
    JsonInput = JsonInput & "      ""billInformation"": ""//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30""" & vbCrLf
    JsonInput = JsonInput & "    }" & vbCrLf
    JsonInput = JsonInput & "  }," & vbCrLf
    JsonInput = JsonInput & "  ""alternativeSchemes"": {" & vbCrLf
    JsonInput = JsonInput & "    ""alternativeSchemeParameters"": [" & vbCrLf
    JsonInput = JsonInput & "      ""Name AV1: UV;UltraPay005;12345""," & vbCrLf
    JsonInput = JsonInput & "      ""Name AV2: XY;XYService;54321""" & vbCrLf
    JsonInput = JsonInput & "    ]" & vbCrLf
    JsonInput = JsonInput & "  }" & vbCrLf
    JsonInput = JsonInput & "}"

    AsyncRequest = True

    Set XMLHTTP = CreateObject("MSXML2.XMLHTTP")
    With XMLHTTP
        .Open "POST", RestUrl, AsyncRequest
        .setRequestHeader "Content-Type", "application/json; charset=utf-8"
        ' The desired output format
        .setRequestHeader "accept", "image/png"
        ' The desired output language
        .setRequestHeader "Accept-Language", "de"
        .Send JsonInput
        While .readyState <> 4
            DoEvents
        Wend
        
        If .Status <> 200 Then
            ' any status other than 200 OK is an error, show it
            MsgBox .responseText, vbExclamation, "Error " & .Status
        Else
            ResponseBytes = .responseBody
            
            ' Write response to a file
            FileNumber = FreeFile
            If Dir(OutputFile) <> "" Then
                Kill OutputFile
            End If
            Open OutputFile For Binary As #FileNumber
            Put #FileNumber, , ResponseBytes
            Close #FileNumber
            
            ' Open File
            ActiveWorkbook.FollowHyperlink OutputFile
        End If
 
    End With
End Sub


