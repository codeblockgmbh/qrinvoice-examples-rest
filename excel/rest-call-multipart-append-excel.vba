Sub Call_RestAPI_PDFAppend()

  Const STR_BOUNDARY As String = "----ExcelVbaFormBoundaryWOi0kPjklOekqkpR"
  Dim RestUrl As String
  Dim AsyncRequest As Boolean
  Dim FileNumber As Long
  Dim ResponseBytes() As Byte
    
  Dim MultipartInputStr As String
  Dim InputFilePath1 As String, InputFilePath2 As String
  
  Dim OutputFile As String
    
  ChDir ActiveWorkbook.Path
  OutputFile = ActiveWorkbook.Path & "/appended.pdf"
    
  ' The following URL points to the demo environment. For production use with individual API key switch URL to https://rest.qr-invoice.cloud/...
  RestUrl = "https://demo.qr-invoice.cloud/v2/pdf/append"
  ' This is an API Key for demonstration purposes. Some values are overridden during create of QR Payment Part & Receipt
  RestUrl = RestUrl & "?api_key=582c9ea9-741a-4bb6-acae-cf92f8805864"

  InputFilePath1 = "C:\temp\a.pdf"
  InputFilePath2 = "C:\temp\b.pdf"
  
  ' build multipart string
  MultipartInputStr = "--" & STR_BOUNDARY & vbCrLf & _
        "Content-Disposition: form-data; name=""file""; filename=""" & Mid$(InputFilePath1, InStrRev(InputFilePath1, "\") + 1) & """" & vbCrLf & _
        "Content-Type: application/pdf" & vbCrLf & _
        vbCrLf & GetFileBytes(InputFilePath1) & vbCrLf & _
        "--" & STR_BOUNDARY & vbCrLf & _
        "Content-Disposition: form-data; name=""file2""; filename=""" & Mid$(InputFilePath2, InStrRev(InputFilePath2, "\") + 1) & """" & vbCrLf & _
        "Content-Type: application/pdf" & vbCrLf & _
        vbCrLf & GetFileBytes(InputFilePath2) & vbCrLf & _
        "--" & STR_BOUNDARY & _
        "--"
        
  AsyncRequest = True
    
  Set XMLHTTP = CreateObject("MSXML2.XMLHTTP")
  With XMLHTTP
    .Open "POST", RestUrl, AsyncRequest
    .setRequestHeader "Content-Type", "multipart/form-data; boundary=" & STR_BOUNDARY
    .setRequestHeader "accept", "application/pdf"
    .send stringToByteArray(MultipartInputStr)
    
    While .readyState <> 4
            DoEvents
    Wend
        
    If .Status <> 200 Then
        ' any status other than 200 OK is an error, show it
        MsgBox .responseText, vbExclamation, "Error " & .Status
    Else
        ResponseBytes = .responseBody
        
        ' Write response to a file
        FileNumber = FreeFile
        If Dir(OutputFile) <> "" Then
            Kill OutputFile
        End If
        Open OutputFile For Binary As #FileNumber
        Put #FileNumber, , ResponseBytes
        Close #FileNumber
        
        ' Open File
        ActiveWorkbook.FollowHyperlink OutputFile
    End If
  End With
  
  Set XMLHTTP = Nothing
End Sub
 

Public Function GetFileBytes(ByVal fPath As String) As String
    Fnum = FreeFile
    Dim bytRtnVal() As Byte
    If LenB(Dir(fPath)) Then ''// is existing file?
        Open fPath For Binary Access Read As Fnum
        ReDim bytRtnVal(LOF(Fnum) - 1&) As Byte
        Get Fnum, , bytRtnVal
        Close Fnum
    Else
        Err.Raise 53
    End If
    GetFileBytes = byteArrayToString(bytRtnVal)
    Erase bytRtnVal
End Function
 
Public Function byteArrayToString(bytArray() As Byte) As String
    Dim sAns As String
        sAns = StrConv(bytArray, vbUnicode)
    byteArrayToString = sAns
 End Function
 
Public Function stringToByteArray(srcTxt As String) As Byte()
    stringToByteArray = StrConv(srcTxt, vbFromUnicode)
End Function

