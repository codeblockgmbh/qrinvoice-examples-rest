Option Compare Database

Private Function EmptyString(str As Variant) As Boolean
    If IsNull(str) Or IsEmpty(str) Then
        EmptyString = True
    Else
        EmptyString = False
    End If
End Function


Private Function NotEmptyString(str As Variant) As Boolean
    NotEmptyString = Not EmptyString(str)
End Function


Private Sub Seitenfußbereich_Format(Cancel As Integer, FormatCount As Integer)

    Dim JsonInput As String
    Dim RestUrl As String
    Dim AsyncRequest As Boolean
    Dim FileNumber As Long
    Dim ResponseBytes() As Byte
    
    ' Target URL of the REST API including the parameter
    ' The following URL points to the demo environment. For production use with individual API key switch URL to https://rest.qr-invoice.cloud/...
    RestUrl = "https://demo.qr-invoice.cloud/v2/payment-part-receipt"
    ' This is an API Key for demonstration purposes. Some values are overridden during create of QR Payment Part & Receipt
    RestUrl = RestUrl & "?api_key=582c9ea9-741a-4bb6-acae-cf92f8805864"
    ' Boundary Lines are not needed when printed on perforated paper
    RestUrl = RestUrl & "&boundaryLineScissors=true"
    RestUrl = RestUrl & "&boundaryLineSeparationText=false"
    RestUrl = RestUrl & "&boundaryLines=true"
    ' On the demo instance, only Liberation Sans is supported
    RestUrl = RestUrl & "&fontFamily=LIBERATION_SANS"
    ' DIN_LANG_CROPPED, DIN_LANG, A5, A4
    ' Cropped returns image to be sized 200 x 100mm -> print margins should be 5mm
    RestUrl = RestUrl & "&pageSize=DIN_LANG_CROPPED"
    ' Resolution only needed for rasterized graphics
    RestUrl = RestUrl & "&resolution=MEDIUM_300_DPI"

    ' The json document contains the information to be rendered
    ' primitive approach in generating a JSON document depending on the given data
    ' PLEASE NOTE: the VBA code here access different report controls
    JsonInput = "{" & vbCrLf
    JsonInput = JsonInput & "  ""creditorInformation"": {" & vbCrLf
    JsonInput = JsonInput & "    ""iban"": """ & Me.CdtrInf_IBAN & """," & vbCrLf
    JsonInput = JsonInput & "    ""creditor"": {" & vbCrLf
    JsonInput = JsonInput & "      ""addressType"": """ & Me.CdtrInf_Cdtr_AdrTp & """," & vbCrLf
    JsonInput = JsonInput & "      ""name"": """ & Me.CdtrInf_Cdtr_Name & """," & vbCrLf
    
    If Me.CdtrInf_Cdtr_AdrTp = "COMBINED" Then
        JsonInput = JsonInput & "    ""addressLine1"": """ & Me.CdtrInf_Cdtr_StrtNmOrAdrLine1 & """," & vbCrLf
        JsonInput = JsonInput & "    ""addressLine2"": """ & Me.CdtrInf_Cdtr_BldgNbOrAdrLine2 & """," & vbCrLf
    Else
        JsonInput = JsonInput & "    ""streetName"": """ & Me.CdtrInf_Cdtr_StrtNmOrAdrLine1 & """," & vbCrLf
        JsonInput = JsonInput & "    ""houseNumber"": """ & Me.CdtrInf_Cdtr_BldgNbOrAdrLine2 & """," & vbCrLf
        JsonInput = JsonInput & "    ""postalCode"": """ & Me.CdtrInf_Cdtr_PstCd & """," & vbCrLf
        JsonInput = JsonInput & "    ""city"": """ & Me.CdtrInf_Cdtr_TwnNm & """," & vbCrLf
    End If
    
    JsonInput = JsonInput & "      ""country"": """ & Me.CdtrInf_Cdtr_Ctry & """" & vbCrLf
    JsonInput = JsonInput & "    }" & vbCrLf
    JsonInput = JsonInput & "  }," & vbCrLf
    JsonInput = JsonInput & "  ""paymentAmountInformation"": {" & vbCrLf
    
    If Not IsNull(Me.CcyAmt_Amt) Then
        JsonInput = JsonInput & "    ""amount"": """ & Me.CcyAmt_Amt & """," & vbCrLf
    End If
    
    JsonInput = JsonInput & "    ""currency"": """ & Me.CcyAmt_Ccy & """" & vbCrLf
    JsonInput = JsonInput & "  }," & vbCrLf
    
    If NotEmptyString(Me.UltmtDbtr_AdrTp) Or _
        NotEmptyString(Me.UltmtDbtr_Name) Or _
        NotEmptyString(Me.UltmtDbtr_StrtNmOrAdrLine1) Or _
        NotEmptyString(Me.UltmtDbtr_BldgNbOrAdrLine2) Or _
        NotEmptyString(Me.UltmtDbtr_PstCd) Or _
        NotEmptyString(Me.UltmtDbtr_TwnNm) Or _
        NotEmptyString(Me.UltmtDbtr_Ctry) _
        Then
        JsonInput = JsonInput & "  ""ultimateDebtor"": {" & vbCrLf
        JsonInput = JsonInput & "    ""addressType"": """ & Me.UltmtDbtr_AdrTp & """," & vbCrLf
        
        JsonInput = JsonInput & "    ""name"": """ & Me.UltmtDbtr_Name & """," & vbCrLf
            
        If Me.UltmtDbtr_AdrTp = "COMBINED" Then
            JsonInput = JsonInput & "    ""addressLine1"": """ & Me.UltmtDbtr_StrtNmOrAdrLine1 & """," & vbCrLf
            JsonInput = JsonInput & "    ""addressLine2"": """ & Me.UltmtDbtr_BldgNbOrAdrLine2 & """," & vbCrLf
        Else
            JsonInput = JsonInput & "    ""streetName"": """ & Me.UltmtDbtr_StrtNmOrAdrLine1 & """," & vbCrLf
            JsonInput = JsonInput & "    ""houseNumber"": """ & Me.UltmtDbtr_BldgNbOrAdrLine2 & """," & vbCrLf
            JsonInput = JsonInput & "    ""postalCode"": """ & Me.UltmtDbtr_PstCd & """," & vbCrLf
            JsonInput = JsonInput & "    ""city"": """ & Me.UltmtDbtr_TwnNm & """," & vbCrLf
        End If
        
        JsonInput = JsonInput & "    ""country"": """ & Me.UltmtDbtr_Ctry & """" & vbCrLf
        JsonInput = JsonInput & "  }," & vbCrLf
    End If
    
    JsonInput = JsonInput & "  ""paymentReference"": {" & vbCrLf
    JsonInput = JsonInput & "    ""referenceType"": """ & Me.RmtInf_Tp & """," & vbCrLf
    
    If NotEmptyString(Me.RmtInf_Ref) And Me.RmtInf_Tp <> "NON" Then
        JsonInput = JsonInput & "    ""reference"": """ & Me.RmtInf_Ref & """," & vbCrLf
    End If
    
    JsonInput = JsonInput & "    ""additionalInformation"": {" & vbCrLf
    JsonInput = JsonInput & "      ""unstructuredMessage"": """ & Me.RmtInf_AddInf_Ustrd & """" & vbCrLf
    
    If NotEmptyString(Me.RmtInf_AddInf_StrdBkgInf) Then
        JsonInput = JsonInput & "," & vbCrLf
        JsonInput = JsonInput & "      ""billInformation"": """ & Me.RmtInf_AddInf_StrdBkgInf & """" & vbCrLf
    End If
    
    JsonInput = JsonInput & "    }" & vbCrLf
    JsonInput = JsonInput & "  }" & vbCrLf

    
    If NotEmptyString(Me.AltPmtInf_AltPmt1) Or NotEmptyString(Me.AltPmtInf_AltPmt2) Then
        JsonInput = JsonInput & "," & vbCrLf
        JsonInput = JsonInput & "  ""alternativeSchemes"": {" & vbCrLf
        JsonInput = JsonInput & "    ""alternativeSchemeParameters"": [" & vbCrLf
        If NotEmptyString(Me.AltPmtInf_AltPmt1) Then
            JsonInput = JsonInput & "      """ & Me.AltPmtInf_AltPmt1 & """" & vbCrLf
        End If
        
        If NotEmptyString(Me.AltPmtInf_AltPmt2) Then
            If NotEmptyString(Me.AltPmtInf_AltPmt1) Then
                JsonInput = JsonInput & "," & vbCrLf
            End If
            JsonInput = JsonInput & "      """ & Me.AltPmtInf_AltPmt2 & """" & vbCrLf
        End If
        JsonInput = JsonInput & "    ]" & vbCrLf
        JsonInput = JsonInput & "  }" & vbCrLf
    End If
    
    
    JsonInput = JsonInput & "}"

    '------------------- end of JSON generation

    AsyncRequest = True

    Set XMLHTTP = CreateObject("MSXML2.XMLHTTP")
    With XMLHTTP
        .Open "POST", RestUrl, AsyncRequest
        .setRequestHeader "Content-Type", "application/json; charset=utf-8"
        ' The desired output format
        .setRequestHeader "accept", "image/png"
        ' The desired output language
        .setRequestHeader "Accept-Language", "de"
        .Send JsonInput
        While .ReadyState <> 4
            DoEvents
        Wend
        
        If .Status <> 200 Then
            ' any status other than 200 OK is an error, show it
            MsgBox .responseText, vbExclamation, "Error " & .Status
        Else
            ResponseBytes = .responseBody
            
            ' Assign the received image to an image control
            Me![PaymentPartReceipt].PictureData = ResponseBytes
        End If
 
    End With
End Sub
