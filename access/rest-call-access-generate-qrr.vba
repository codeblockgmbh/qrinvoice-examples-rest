Option Compare Database

Private Function EmptyString(str As Variant) As Boolean
    If IsNull(str) Or IsEmpty(str) Then
        EmptyString = True
    Else
        EmptyString = False
    End If
End Function


Private Function NotEmptyString(str As Variant) As Boolean
    NotEmptyString = Not EmptyString(str)
End Function


Private Sub generateQrReference()

    If NotEmptyString(Me.Nr.Value) Then
           
        Dim RequestBody As String
        Dim RestUrl As String
        Dim AsyncRequest As Boolean
        Dim ResponseBytes() As Byte
        
		' Target URL of the REST API including the parameter
		' The following URL points to the demo environment. For production use with individual API key switch URL to https://rest.qr-invoice.cloud/...
		RestUrl = "https://demo.qr-invoice.cloud/v2/qr-reference/create"
		' This is an API Key for demonstration purposes.
		RestUrl = RestUrl & "?api_key=582c9ea9-741a-4bb6-acae-cf92f8805864"
        If NotEmptyString(Me.BESR_ID.Value) Then
            RestUrl = RestUrl & "&customerId=" & Me.BESR_ID.Value
        End If
        
        RequestBody = Me.Nr.Value
    
        AsyncRequest = True
    
        Set XMLHTTP = CreateObject("MSXML2.XMLHTTP")
        With XMLHTTP
            .Open "POST", RestUrl, AsyncRequest
            .setRequestHeader "Content-Type", "text/plain"
            .setRequestHeader "accept", "text/plain"
            .Send RequestBody
            While .ReadyState <> 4
                DoEvents
            Wend
            
            If .Status <> 200 Then
                ' any status other than 200 OK is an error, show it
                MsgBox .responseText, vbExclamation, "Error " & .Status
            Else
                ResponseBytes = .responseBody
                
                ' Assign the received qr reference
                Me.QR_Referenz.Value = .responseText
            End If
     
        End With
    End If
End Sub
