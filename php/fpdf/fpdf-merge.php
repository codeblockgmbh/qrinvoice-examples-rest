<?php

// this code example uses FPDF with FPDI
// please note that this example is a simplified one as it does not implement any error handling

use setasign\Fpdi\Fpdi;

require __DIR__ . '/vendor/autoload.php';

$pdf = new FPDI();

// before printing anything we import the input document
// set the source to a previously by the QR Invoice Cloud REST Services generated PDF file (A4)
// see other PHP example on how to call the REST service
$pagecount = $pdf->setSourceFile("input/paymentpartreceipt.pdf");

// import page 1 (there is only 1 page)
$tppl = $pdf->importPage(1);

// use this page as template
// use the imported page and place it at point 0,0 (lower left corner) with a width of 210 mm
$pdf->AddPage();
$pdf->useTemplate($tppl, 0, 0, 210);

// create first page with some output - this will be the invoice without the Payment Part & Receipt from the QR-bill.
$pdf->SetFont('Helvetica', '', 16);
$pdf->SetTextColor(0, 0, 0);
$pdf->SetXY(10, 30);
$pdf->Write(0, "This will be the invoice");

// finally write the single paged PDF file as the QR-Bill
$pdf->Output("output/qrbill-one-page.pdf", "F");

?>