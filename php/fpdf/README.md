# QR Invoice PHP / FPDF Examples

## Install dependencies

    php composer.phar install

## Execute examples

### Append Payment Part & Receipt - 2 Pages PDF

    php fpdf-append.php

### Merge Payment Part & Receipt with Invoice - 1 Page PDF

    php fpdf-merge.php
