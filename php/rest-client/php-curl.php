<?php

// this example uses PHPs curl functions to make the REST call, however any other way for REST calls can be used
// please note that this example is a simplified one as it does not implement any error handling - make sure you do so for production use (see the Swagger Documentation for details on HTTP status codes)

$output_language = 'de'; // de, fr, it, en
$output_format = 'application/pdf'; // this example is for PDF only, however also rasterized images are supported: image/png ...
$font_family = 'LIBERATION_SANS'; // HELVETICA, ARIAL, LIBERATION_SANS
$page_size = 'A4'; // A4, A5, DIN_LANG
$boundary_lines = 'true'; // true, false
$boundary_line_scissors = 'true'; // true, false
$boundary_line_separation_label = 'true'; // true, false - can only be true for A4 and A5 output

// The following URL points to the demo environment. For production use with individual API key switch URL to https://rest.qr-invoice.cloud/...
$base_url = 'https://demo.qr-invoice.cloud';
// This is an API Key for demonstration purposes. Some values are overridden during create of QR Payment Part & Receipt
$api_key = '582c9ea9-741a-4bb6-acae-cf92f8805864'; 

$service_url = "$base_url/v2/payment-part-receipt" .
    "?api_key=$api_key" .
    "&fontFamily=$font_family" .
    "&pageSize=$page_size" .
    "&boundaryLines=$boundary_lines" .
    "&boundaryLineScissors=$boundary_line_scissors" .
    "&boundaryLineSeparationText=$boundary_line_separation_label";

$request = array(
    'creditorInformation' =>
        array(
            'creditor' =>
                array(
                    'addressType' => 'STRUCTURED',
                    'city' => 'Biel',
                    'country' => 'CH',
                    'houseNumber' => '1268',
                    'name' => 'Robert Schneider AG',
                    'postalCode' => '2501',
                    'streetName' => 'Rue du Lac',
                ),
            'iban' => 'CH4431999123000889012'
        ),
    'ultimateDebtor' =>
        array(
            'addressType' => 'STRUCTURED',
            'city' => 'Rorschach',
            'country' => 'CH',
            'houseNumber' => '28',
            'name' => 'Pia-Maria Rutschmann-Schnyder',
            'postalCode' => '9400',
            'streetName' => 'Grosse Marktgasse',
        ),
    'paymentAmountInformation' =>
        array(
            'amount' => 1949.75,
            'currency' => 'CHF',
        ),
    'paymentReference' =>
        array(
            'reference' => '210000000003139471430009017',
            'referenceType' => 'QRR',
            'additionalInformation' =>
                array(
                    'unstructuredMessage' => 'Instruction of 15.09.2019',
                    'billInformation' => '//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30',
                ),
        ),
    'alternativeSchemes' =>
        array(
            'alternativeSchemeParameters' =>
                array(
                    0 => 'Name AV1: UV;UltraPay005;12345',
                    1 => 'Name AV2: XY;XYService;54321',
                ),
        ),
);

$curl = curl_init($service_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept-Language: $output_language", "Accept: $output_format", 'Content-Type: application/json'));

$curl_response = curl_exec($curl);
if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
}

if (!curl_errno($curl)) {
    switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
      case 200:  # OK
        $payment_part_receipt_file = 'output/paymentpartreceipt.pdf';
        file_put_contents($payment_part_receipt_file, $curl_response);
        break;
      default:
        echo "Unexpected error HTTP-Code: $http_code \n" 
        . "Response: $curl_response";
    }
} else {
    die('TODO');
}

curl_close($curl);

?>
