# QR Invoice PHP / curl example

## Execute example

    php php-curl.php

## SSL Certificate Problem on Windows

In case you are working on Windows with PHP an experiencing an SSL issue accessing the REST endpoints on APPUiO which is secured using Let's Encrypt, please see the following page:
https://community.letsencrypt.org/t/what-means-ssl-certificate-problem-unable-to-get-local-issuer-certificate/12517/2
